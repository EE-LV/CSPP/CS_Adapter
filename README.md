# CS_Adapter

This library contains a collection of CS++ adapter classes to the CS Framework.
Author: H.Brand@gsi.de, D.Neidherr@gsi.de
Copyright 2021  GSI Helmholtzzentrum für Schwerionenforschung GmbH